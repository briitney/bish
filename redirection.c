#include <stdio.h>
#include <stdlib.h>

// For using strcmp
#include <string.h>

// For using exec
#include <unistd.h>

// For using readline
#include <readline/readline.h>
#include <readline/history.h>

#include <fcntl.h>

char** strtok_arr(char* a_string, char* delimeter);
int get_occurances(char* line, char* delimiter);
void redirect(char** args);
int string_arr_loc(char* string, char** arr);
char* remove_from_array(char* string, char** arr);

int main (int argc, char* argv[]) {
	//char * input = (char *)"cat < input | sort > output";
	char input[] = "cat < bag-o-dicks > output";
	char** args = strtok_arr(input, " ");
	redirect(args);
	execv("/usr/bin/cat", args);
}

char** strtok_arr(char* a_string, char* delimeter) {
	if (strcmp(a_string, "") == 0) return NULL;
	int results_length = get_occurances(a_string, delimeter)+2;
	char** results;
	results = malloc(results_length * sizeof(char*));
	char* a_word = strtok(a_string, delimeter);
	int c = 0;
	while (1) {
		if (a_word == NULL) break;
		results[c] = a_word;
		a_word = strtok(NULL, delimeter);
		c++;
	}
	results[results_length-1] = NULL;
	return results;
}

int get_occurances(char* line, char* delimiter) {
	int i = 0;
	int occurances = 0;
	while (1) {
		if (line[i] == '\0') break;
		if (strncmp(line+i, delimiter, strlen(delimiter)) == 0) occurances++;
		i++;
	}
	return occurances;
}

void redirect(char** args) {
	int loc = string_arr_loc("<", args);
	if (loc != -1) {
		char* input_filename = args[loc+1];
		remove_from_array("<", args);
		remove_from_array(input_filename, args);
		int in_fd = open(input_filename, O_RDONLY);
		if (in_fd < 0) perror("Could not redirect file input");
		int err = dup2(in_fd, 0);
	}
	loc = string_arr_loc(">", args);
	if (loc != -1) {
		char* output_filename = args[loc+1];
		remove_from_array(">", args);
		remove_from_array(output_filename, args);
		int out_fd = open(output_filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
		if (out_fd < 0) perror("Could not redirect output");
		int err = dup2(out_fd, 1);
	}
}

int string_arr_loc(char* string, char** arr) {
	int count = 0;
	int retval = -1;
	char* current = arr[0];
	while (current != NULL) {
		if (strcmp(current, string) == 0) {
			retval = count;
			break;
		}
		current = arr[++count];
	}
	return retval;
}

char* remove_from_array(char* string, char** arr) {
	int loc = string_arr_loc(string, arr);
	char* retval = arr[loc];
	while (arr[loc] != NULL) {
		arr[loc] = arr[loc+1];
		loc++;
	}
	return retval;
}
