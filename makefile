Main=	main
Program=	BISH

all:
	make $(Program)

test: $(Program)
	./$(Program)

$(Program):	$(Main).c $(Main).h makefile
	gcc $(Main).c -o $(Program) -lreadline

clean:
	touch $(Main).c make

