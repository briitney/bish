#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>

// For finding the size of a file
#include <sys/stat.h>

// For using strcmp
#include <string.h>

// For using exec
#include <unistd.h>

// For using waitpid
#include <sys/wait.h>

// For using readline
#include <readline/readline.h>
#include <readline/history.h>

// For file writing permissions
#include <fcntl.h>

void fetch_input();
void interpret(char* input);
void execute(char** input);
void execute_path (char** input);
char** get_paths();
void execute_absolute (char* absolute, char** input);
int get_occurances(char* line, char* delimiter);
char** strtok_arr(char* a_string, char* delimeter);
void clean_arr(char** arr);
char* concat(char** strings);
char* get_first_arg(char* string);
void piping(char* input);
void check(int result, char* err_msg, int lethal);
char* strtokm(char *str, const char *delim);
int* redirection_fds(char* command);
void remove_redirects(char** args);
int string_arr_loc(char* string, char** arr);
char* remove_from_array(char* string, char** arr);

#endif

