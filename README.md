### Assignment for CS426, Operating Systems
[Randy's grading page](http://euclid.nmu.edu/~rappleto/Classes/CS426/TheBigAssignment/TheBigAssignment.txt)

Must get 20 out of 40 points, no more than 20

Bold are accomparehed tasks, italics are planned tasks

Current points: 20

The Shell Assignment (total 42 points)

**1 Can run an executable**  
&nbsp;&nbsp;&nbsp;&nbsp;/bin/ls  
**1 You search the path for the executable**  
&nbsp;&nbsp;&nbsp;&nbsp;ls  
**1 Can do file input redirection "<"**  
&nbsp;&nbsp;&nbsp;&nbsp;ls > fred  
**1 Can do file output redirection ">"**  
&nbsp;&nbsp;&nbsp;&nbsp;ls < fred  
**2 Can do command piping "|"**  
&nbsp;&nbsp;&nbsp;&nbsp;ls | wc  
**+1 Can do lots of pipes**  
&nbsp;&nbsp;&nbsp;&nbsp;ls | grep fred | wc  
**1 Can do at least one combination of these things**  
&nbsp;&nbsp;&nbsp;&nbsp;ls | wc > fred  
**4 Can do any combination of three of <, >, and |**   
&nbsp;&nbsp;&nbsp;&nbsp;cat < filename | sort > sortedFile.txt  
2 Can set enviornment variables with your own code  
&nbsp;&nbsp;&nbsp;&nbsp;PATH=:/bin:/sbin:/usr/sbin:/usr/bin  
1 Can set enviornment variables with using putenv/setenv  
&nbsp;&nbsp;&nbsp;&nbsp;PATH=:/bin:/sbin:/usr/sbin:/usr/bin  
2 Expands enviornment variables on the command line  
&nbsp;&nbsp;&nbsp;&nbsp;ls $HOME  
2 Does filename expansion "glob" (Hint:&nbsp;&nbsp;&nbsp;&nbsp;Use the built in glob.)  
&nbsp;&nbsp;&nbsp;&nbsp;ls a\*b  
**1 Knows how to change directory**  
&nbsp;&nbsp;&nbsp;&nbsp;cd /fred  
1 Bang last command   
&nbsp;&nbsp;&nbsp;&nbsp;Runs the last command that started with that letter  
&nbsp;&nbsp;&nbsp;&nbsp;!r runs rm (the last command that started with an 'r')  
1 Bang # command  
&nbsp;&nbsp;&nbsp;&nbsp;!4 runs 4th command from history  
**1 Queue commands**  
&nbsp;&nbsp;&nbsp;&nbsp;make ; make install  
**+1 Can have lots of semicolons**  
&nbsp;&nbsp;&nbsp;&nbsp;ls; sleep 3; rm fred  
**2 Change Prompt**  
&nbsp;&nbsp;&nbsp;&nbsp;PS1="what is you command?"  
3 Can run commands in the background.  
&nbsp;&nbsp;&nbsp;&nbsp;processImage &  
1 Concatenate commands with &.&nbsp;&nbsp;&nbsp;&nbsp;Only runs next command if the previous comand returned success.  
&nbsp;&nbsp;&nbsp;&nbsp;cd /home/rappleto & rm fred.txt  
1 Catch Keyboard interrupt  
&nbsp;&nbsp;&nbsp;&nbsp;ctrl + c = back to prompt  
1 Replace "~" with the home directory  
&nbsp;&nbsp;&nbsp;&nbsp;rm ~/junkfile  
1 Control-L clears the screen  
&nbsp;&nbsp;&nbsp;&nbsp;ctrl-l = clear screen  
3 When they misspell a command, offer a suggestion  
&nbsp;&nbsp;&nbsp;&nbsp;(user) lss  
&nbsp;&nbsp;&nbsp;&nbsp;(shell) Did you mean "ls"?  
2 Can run commands from a file  
&nbsp;&nbsp;&nbsp;&nbsp;. scriptFile.txt  
**2 Tab Completion and Arrow History**  
1 Saves and reloads history to a file  
2 Aliases work.  
&nbsp;&nbsp;&nbsp;&nbsp;alias dir=ls  
2 Automatically runs a file called .myshell when it starts  
2 Only runs execuatables from an approved list  
**1 Prompt has hostname or username.**  
-2 Commands cannot have arguments (i.e. ls -l does not work).  
  


