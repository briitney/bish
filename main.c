#include "main.h"

char* prompt;
extern char** environ;

int main (int argc, char* argv[]) {
	char ** prompt_bits = malloc(5 * sizeof(char*));
	prompt_bits[0] = "§[";
	prompt_bits[1] = getenv("USER");
	prompt_bits[2] = "]§";
	prompt_bits[3] = " (◕◡◕✿) »»» ";
	prompt_bits[4] = NULL;
	prompt = concat(prompt_bits);
	free(prompt_bits);
	while (1) {
		fetch_input();
	}
}

void fetch_input() {
	char* line = readline(prompt);
	if (strcmp(line, "") == 0) return;
	add_history(line);
    char** lines = strtok_arr(line, ";");
	int i = 0;
	char* a_line = lines[i];
	while (a_line != NULL) {
		interpret(lines[i]);
		a_line = lines[++i];
	}
	free(lines);
	free(line);
}

void interpret(char* input) {
	char* first_arg = get_first_arg(input);
	if (input == NULL) {
		return;
	} else if (strcmp(first_arg, "exit") == 0) {
		printf("Goodbye❣ ~\n");
		exit(0);
	} else if ((strcmp(first_arg, "OwO") == 0) || (strcmp(first_arg, "owo") == 0)) {
		printf("\t*notices memory leaks*\n\tOwO\tWhat's this?\n");
	} else if (strcmp(first_arg, "cd") == 0) {
		char** args = strtok_arr(input, " ");
		check(chdir(args[1]) < 0, "Could not change directory", 0);
	} else if (strncmp(input, "prompt=", 7) == 0) {
		char** promptstuff = strtok_arr(input, "\"");
		prompt = promptstuff[1];
		prompt = strdup(prompt);
		free(promptstuff);
	} else {
		pid_t pid = fork();
		if (pid == 0) {
			piping(input);
			exit(0);
		} else {
			int n;
			waitpid(pid, &n, 0);
		}
	}
	free(first_arg);
}

void piping(char* input) {
    int i = 0;
    pid_t pid;
	int dev_null = open("/dev/null", O_RDONLY);
	if (dev_null < 0) {
		perror("Could not open /dev/null");
	}
	char** commands = strtok_arr(input, "|");
	int numPipes = get_occurances(input, "|");
    int pipefds[2*numPipes];
    for(i = 0; i < numPipes; i++) {
		check(pipe(pipefds + i*2) < 0, "Couldn't pipe", 1);
    }
    for(i = 0; i <= 2*numPipes; i+=2) {
		int* redirects = redirection_fds(commands[i/2]);
		if (i > 0) {
			if (redirects[0] != 0) {
				close(pipefds[i-2]);
				pipefds[i-2] = redirects[0];
			}
		} else {
			if (redirects[0] != 0) {
				check(dup2(redirects[0], 0) < 0, "Couldn't pipe", 1);
			}
		}
		if (i < numPipes*2) {
			if (redirects[1] != 1) {
				close(pipefds[i]);
				pipefds[i] = dev_null;
				close(pipefds[i+1]);
				pipefds[i+1] = redirects[1];
			}
		} else {
			if (redirects[1] != 1) {
				check(dup2(redirects[1], 1) < 0, "Couldn't pipe", 1);
			}
		}
	}
    int fd_indx = 0;
    int com_cnt = 0;
    while(commands[com_cnt]) {
		char** args = strtok_arr(commands[com_cnt], " ");
		remove_redirects(args);
        pid = fork();
        if(pid == 0) {
            if(commands[com_cnt+1]){
				check(dup2(pipefds[fd_indx+1], 1) < 0, "Couldn't pipe", 1);
            }
            if(com_cnt != 0){
				check(dup2(pipefds[fd_indx-2], 0) < 0, "Couldn't pipe", 1);
			}
            for(i = 0; i < 2*numPipes; i++) close(pipefds[i]);
			execute(args);
        } else check(pid < 0, "Couldn't pipe", 1);
        com_cnt++;
        fd_indx+=2;
    }
    for(i = 0; i < 2 * numPipes; i++) close(pipefds[i]);
    int status;
    for(i = 0; i < numPipes + 1; i++) wait(&status);
	free(commands);
}

void check(int result, char* err_msg, int lethal) {
	if (result) {
		perror(err_msg);
		if (lethal) {
			exit(0);
		}
	}
}

void execute(char** input) {
	if (input[0][0] == '/') {
		execute_absolute(input[0], input);
	}
	execute_path(input);
}

void execute_path (char** input) {
	char** paths = get_paths();
	int i = 0;
	char* a_path = paths[i];
	while (a_path != NULL) {
		char ** args;
		args = malloc(4 * sizeof(char*));
		args[0] = a_path;
		args[1] = (char *)"/";
		args[2] = input[0];
		args[3] = NULL;
		char* an_absolute_path = concat(args);
		execute_absolute(an_absolute_path, input);
		a_path = paths[++i];
	}
	fprintf( stderr, "Could not find executable '%s'.\n", input[0]);
	exit(0);
}

void execute_absolute (char* absolute, char** input) {
	execv(absolute, input);
}

char** get_paths() {
	int i = 0;
	char* path;
	while(environ[i++]) {
		if (strncmp(environ[i], "PATH=", 5) == 0) {
			path = &(environ[i])[5];
			break;
		}
	}
	if (environ[i] == NULL) {
		fprintf(stderr, "Could not find PATH variable. Exiting...");
		exit(1);
	}
	return strtok_arr(getenv("PATH"), ":");
}

int get_occurances(char* line, char* delimiter) {
	int i = 0;
	int occurances = 0;
	while (1) {
		if (line[i] == '\0') break;
		if (strncmp(line+i, delimiter, strlen(delimiter)) == 0) occurances++;
		i++;
	}
	return occurances;
}

char** strtok_arr(char* original_string, char* delimeter) {
	char* a_string = strdup(original_string);
	if (strcmp(a_string, "") == 0) return NULL;
	int results_length = get_occurances(a_string, delimeter)+2;
	char** results;
	results = malloc(results_length * sizeof(char*));
	if (strlen(a_string) <= strlen(delimeter)) {
		results[0] = a_string;
		results[1] = NULL;
		return results;
	}
	char* a_word = strtokm(a_string, delimeter);
	int c = 0;
	while (1) {
		if (a_word == NULL) break;
		results[c] = a_word;
		a_word = strtokm(NULL, delimeter);
		c++;
	}
	results[results_length-1] = NULL;
	clean_arr(results);
	return results;
}

void clean_arr(char** arr) {
	int test = string_arr_loc("", arr);
	while (test != -1) {
		remove_from_array("", arr);
		test = string_arr_loc("", arr);
	}
}

char* concat(char** strings) {
	int size = 0;
	int i = 0;
	char* a = strings[i];
	while (a != NULL) {
		size += sizeof(a);
		a = strings[++i];
	}
	char* result = (char *)calloc(size+1, sizeof* result);
	i = 1;
	a = strings[1];
	strcpy(result, strings[0]);
	while (a != NULL) {
		strcat(result, strings[i]);
		a = strings[++i];
		if (strings[i] == NULL) break;
	}
	return result;
}

char* get_first_arg(char* string) {
	char* string_copy = strdup(string);
	char* kinda_retval = strtok(string_copy, " ");
	char* retval = strdup(kinda_retval);
	free(string_copy);
	return retval;
}

char* strtokm(char *str, const char *delim) {
	// Needed a strtok that had a multi-character delimeter, from https://stackoverflow.com/a/29848367
    static char* tok;
    static char* next;
    char *m;
    if (delim == NULL) return NULL;
    tok = (str) ? str : next;
    if (tok == NULL) return NULL;
    m = strstr(tok, delim);
    if (m) {
        next = m + strlen(delim);
        *m = '\0';
    } else {
        next = NULL;
    }
    return tok;
}

int* redirection_fds(char* command) {
	char** args = strtok_arr(command, " ");
	int loc = string_arr_loc("<", args);
	int* retval = malloc(2 * sizeof(int));
	retval[0] = 0;
	retval[1] = 1;
	if (loc != -1) {
		char* input_filename = args[loc+1];
		int in_fd = open(input_filename, O_RDONLY);
		check(in_fd < 0, "Could not open input for redirection", 1);
		retval[0] = in_fd;
	}
	loc = string_arr_loc(">", args);
	if (loc != -1) {
		char* output_filename = args[loc+1];
		int out_fd = open(output_filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
		if (out_fd < 0) {
			perror("Could not open output for redirection");
			out_fd = 1;
		}
		retval[1] = out_fd;
	}
	return retval;
}

void remove_redirects(char** args) {
	int loc = string_arr_loc("<", args);
	if (loc != -1) {
		char* input_filename = args[loc+1];
		remove_from_array("<", args);
		remove_from_array(input_filename, args);
	}
	loc = string_arr_loc(">", args);
	if (loc != -1) {
		char* output_filename = args[loc+1];
		remove_from_array(">", args);
		remove_from_array(output_filename, args);
	}
}

int string_arr_loc(char* string, char** arr) {
	int count = 0;
	int retval = -1;
	char* current = arr[0];
	while (current != NULL) {
		if (strcmp(current, string) == 0) {
			retval = count;
			break;
		}
		current = arr[++count];
	}
	return retval;
}

char* remove_from_array(char* string, char** arr) {
	int loc = string_arr_loc(string, arr);
	char* retval = arr[loc];
	while (arr[loc] != NULL) {
		arr[loc] = arr[loc+1];
		loc++;
	}
	return retval;
}
